# Brikit Demos

Brikit demos allows stand-alone Brikit demo sites to easily be provisions for Brikit customers.

Repo includes configuration files and tests for:

* building a Brikit demos stack via cloud formation
* updating stack to support new demo sites

# Dependencies

yac!

pip install yac

# Use Cases

## Build a stack

The following command will build the entire demo stack. The stack includes containers that implement the Brikit "demos" instances and data backups. The containers are orchestrated through ECS.

```
yac stack <repo path>/Servicefile.json
```

The command will prompt you for:
	* the environment you are building for, and
	# the password for the RDS sys admin user, and
	* the names of the demo sites

The command will block until the stack print completes.

After the print completes, the command will prompt you for the password of the db user Confluence will use to connect to its DB.

The command will then setup the DB and print instructions needed to complete the site setup.

## Add a New Demo Site

Re-run the yac stack command:

```
yac stack <repo path>/Servicefile.json
```

The command will prompt you for:
	* the environment you are updating, and
	* the names of the demo sites

The command will block until the stack update completes.

After the update completes, the command will prompt you for the password of the RDS sysadmin password.

The command will then setup the DB and print instructions needed to complete the site setup.

## Take a 'snap' backup

Backups of the home directories of all site are scheduled to occur at 7:00 am daily.

To force a backup (aka "snap" backup), run the following:

```
yac container start <path>/Servicefile.json backups --public --cmd="python -m src.backup s3://brikit-internal-yac-apps/demos/<env>/backups.json demos-<env> --snap"
```

The command will prompt you for:
	* the environment you are backing up
