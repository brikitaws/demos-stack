#!/bin/bash

# *****************************************************************
# Load and run file used for bootstrapping a new confluence host.
# Note that this stuff ONLY gets run at instance creation time.
# *****************************************************************

# log stdout and stderr from this script to a boot log
# You can troubleshoot boot issues by inspecting this log file.
logfile=/tmp/boot.log
exec > $logfile 2>&1

# params in curly brackets and CF Refs get rendered in via yac/lib/boot.py

# ntp-servers are defined in vpc defs
NTP_SERVERS="{{ntp-servers}}"

# theres are auto-filled for all yac services based on cli inputs
HOST_NAME={{host-name}}

# Cluster name are defined in the servicefile for confluence and get loaded here via a stack parameter
CLUSTER_NAME={"Ref": "ECS"}

# Restore configs tell the backup container what files to restore at instance boot.
# The configs are the same as the backup configs (restore is backup run backwards!)
# Configs are defined in the servicefile for confluence and get loaded here via a stack parameter
RESTORE_CONFIG={"Ref": "RestoreConfigs"}

# proxy configs are typically defined in the vpc prefs
HTTP_PROXY={{http-proxy}}
https_proxy={{https-proxy}}

# format volumes as an ext4 fs
mkfs -t ext4 /dev/xvdc
mkfs -t ext4 /dev/xvdd

# create the confluence home and docker lib directories
# each will become the mount points on the volumes above.
mkdir -p /var/local/atlassian
mkdir -p /var/lib/docker

# mount the volumes
mount /dev/xvdc /var/lib/docker
mount /dev/xvdd /var/local/atlassian

# recursively create the home dir for confluence. include attachments
# as it will be mounted on EFS shortly
mkdir -p /var/local/atlassian/confluence

# create a subdirectory for each confluence demo 'slot'
# Each slot serves as home for a specific Confluence instance
for ((i=1;i<=10;i++));
do
   mkdir /var/local/atlassian/confluence/$i
   chmod 777 /var/local/atlassian/confluence/$i
done

# create a directory under home for backups to log to
mkdir /var/local/atlassian/confluence/backup-logs

# change owner to daemon
chown -R daemon:daemon /var/local/atlassian/confluence

# give r/w/x(traversal) to everyone under confluence home
# (note: this needs to happen AFTER the chown above)
chmod 777 -R /var/local/atlassian/confluence

echo "NTP setup starting ..."

# configure NTP
if [ $NTP_SERVERS ]; then
	# break existing symbolic link to shared conf file
	rm /etc/ntp.conf
	# write the servers into a new conf file. servers are in a semicolon delimitted string.
	# sed 'em into a conf file with one line per server
	echo $NTP_SERVERS | sed -e 's/;/\n/g' | sed -e 's/^/server /' > /etc/ntp.conf
	chmod 0666  /etc/ntp.conf
	# restart the ntp daemon so it picks up the new configs
	systemctl restart ntpd

	echo "NTP setup complete ..."
fi

# set timezone
timedatectl set-timezone America/Los_Angeles

echo "Remote api setup starting ..."

# create the docker-tcp.socket file, which enables docker's remote API on coreos
echo '[Unit]
	Description=Docker Socket for the API

	[Socket]
	ListenStream=5555
	BindIPv6Only=both
	Service=docker.service

	[Install]
	WantedBy=sockets.target' > /etc/systemd/system/docker-tcp.socket

echo "Remote api setup complete ..."

chmod 0644  /etc/systemd/system/docker-tcp.socket

# reload and restart docker to pick up with the new proxy and docker configurations
systemctl enable docker-tcp.socket
systemctl stop docker
systemctl daemon-reload
systemctl start docker-tcp.socket
systemctl start docker

# restart update-engine to pick up any proxy configs
systemctl restart update-engine

echo "Restore setup start ..."

if [ $RESTORE_CONFIG ]; then

	# restore any home directory files that this instance should have at startup
	docker run --name restore-at-boot \
	--detach=true \
	--volume=/var/local/atlassian/confluence:/var/local/atlassian/confluence:rw \
	--volume=/var/local/atlassian/confluence/backup-logs:/var/local/backups:rw \
	nordstromsets/backups:latest \
	python -m src.restore $RESTORE_CONFIG $HOST_NAME

	echo "Restore setup complete..."
fi

echo "Cluster setup start ..."

if [ $CLUSTER_NAME ]; then

	# install ECS agent and register this instance with the CLUSTER_NAME cluster
	docker run --name ecs-agent \
	--detach=true \
	--restart=on-failure:10 \
	--volume=/var/run/docker.sock:/var/run/docker.sock \
	--volume=/var/log/ecs/:/log \
	--volume=/var/lib/ecs/data:/data \
	--volume=/sys/fs/cgroup:/sys/fs/cgroup:ro \
	--volume=/var/run/docker/execdriver/native:/var/lib/docker/execdriver/native:ro \
	--publish=127.0.0.1:51678:51678 \
	--env=ECS_LOGFILE=/log/ecs-agent.log \
	--env=ECS_LOGLEVEL=debug \
	--env=ECS_DATADIR=/data \
	--env=ECS_DOCKER_GRAPHPATH=/run/docker \
	--env=ECS_CLUSTER=$CLUSTER_NAME \
	amazon/amazon-ecs-agent:latest

	echo "Cluster setup complete..."
fi
