import re, boto3, os, json, jmespath, sys, argparse
from os.path import expanduser
from urlparse import urlparse
from yac.lib.inputs import get_inputs, get_stack_service_inputs_value, pp_list
from yac.lib.inputs import get_image_versions, string_validation
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists, get_ec2_ips
from yac.lib.template import apply_stemplate
from yac.lib.container.api import get_connection_str
from yac.lib.container.build import build_image, get_image_name, get_image_versions_local
from yac.lib.container.build import download_dockerfile
from yac.lib.state import load_state_s3, save_state_s3
from yac.lib.version import is_first_arg_latest, is_a_version, is_same

# populate dynamic params for the jira service
def get_value(params):

    print "gathering inputs via <servicefile>/lib/inputs.py ..."

    # get inputs per service-inputs in the Servicefile
    get_inputs(params)

    # set the default catalina options
    set_catalina_opts(params)

    # get the state of the current set of demo sites into params
    sites = load_sites(params)

    # prompt user to make updates.
    # only prompt for 'yac stack' commands
    if sys.argv[0]=='stack':
        prompt_sites(params)

    # set fqdns for each slot
    set_slot_fqdns(params)

    # set confluence versions for each slot
    set_slot_versions(params)

def set_slot_fqdns(params):
    # The Confluence Dockerfile uses the FQDN to execute http-to-https
    # rewrites

    sites = get_variable(params,'sites',[])

    tl_domain = get_variable(params,'tl-domain')
    sl_domain = get_variable(params,'sl-domain')

    for site in sites:

        cname = site['cname']
        fqdn_key = "fqdn-slot%s"%site['slot']

        fqdn = "%s.%s.%s"%(cname,sl_domain,tl_domain)

        set_variable(params,
                     fqdn_key,
                     fqdn,
                     "FQDN for confluence instance in slot %s"%site['slot'])


def set_slot_versions(params):

    sites = get_variable(params,'sites',[])

    version_lookup_map = {}

    for site in sites:

        version_key = "version-slot%s"%site['slot']

        # print "%s: %s"%(version_key, site['version'])

        set_variable(params,
                     version_key,
                     site['version'],
                     "version for confluence instance in slot %s"%site['slot'])

        version_lookup_map['slot%s'%site['slot']] = site['version']

    # if this is a container build, also add the image-version tag.
    # this tag sets the confuence version in the Dockerfile
    if sys.argv[0]=='build':

        # 2cd arg to the 'yac container build' command sets which slot is being built
        slot_built = sys.argv[2]

        # print "%s, %s, %s"%("image-version",slot_built, version_lookup_map[slot_built])

        set_variable(params,
                     "image-version",
                     version_lookup_map[slot_built],
                     "version for confluence dockerfile")

def set_catalina_opts(params):

    # core options
    CATALINA_ARGS = (
         "-Xms{{confluence-heap-size}}\n" +
         "-Xmx{{confluence-heap-size}}\n" 
         "-Datlassian.plugins.enable.wait=600\n" +
         "-Duser.timezone={{time-zone}}\n")

    # add core jvm-related variables
    catalina_options = get_catalina_options(CATALINA_ARGS)
    catalina_options = apply_stemplate(catalina_options,params)

    set_variable(params,'catalina-opts', catalina_options, "JVM params via catalina options")

           # get constants associated with yac apps
def get_catalina_options(catalina_args_str):

    # convert carriage returns to spaces
    catalina_options = catalina_args_str.replace('\n',' ')

    return catalina_options


def prompt_sites(params):

    sites = get_variable(params,'sites',[])

    in_use = jmespath.search("[?state=='in_use']",sites)
    not_in_use = jmespath.search("[?state=='available']",sites)

    print "\nThe following demo sites are currently in use:\n%s"%pp_site_list(in_use)

    if len(not_in_use)>0:
        print "There are currently %s sites not yet provisioned\n"%len(not_in_use)

    create_new_site = False
    update_existing_site = False

    while True:

        if len(not_in_use)>0:

            create_new_site = site_change_prompter("Create a new demo site? (y/n/<enter>) >> ")

            if create_new_site:
                new_site_wizard(sites, params)

        if len(in_use)>0:

            update_existing_site = site_change_prompter("Update an existing demo site? (y/n/<enter>) >> ")

            if update_existing_site:
                update_site_wizard(sites, params)

        # if user declined opportunity to either create a new site or update an existing site
        # break out of the update loop
        if not create_new_site and not update_existing_site:
            break

    dry_run = ('-d' in sys.argv or '--dryrun' in sys.argv)
    sites_changed = create_new_site or update_existing_site

    # if mods where made and this is not a dry run, save sites back to s3
    if (sites_changed and not dry_run):
        save_sites(params,sites)

def new_site_wizard(sites, params):

    # find a free site
    for site in sites:

        if site['state']=="available":

            # prompt for cname
            site['cname'] = raw_input("Enter cname for this site >> ")

            # prompt for version
            site['version'] = version_wizard(site, params)

            # set state to "setup"
            site['state'] = "setup"

            break

    return site

def update_site_wizard(sites, params):

    # prompt for site cname
    site = {}
    first_try = True
    selected_sites = sites

    while not site:

        msg = ""
        if not first_try:
            msg = "Entry doesn't match an existing cname ...\n"

        old_cname = raw_input(msg + "Enter cname for site to be replaced >> ")

        selected_sites = jmespath.search("[?cname=='%s']"%old_cname,sites)

        if len(selected_sites) >= 1:
            # site is the first site returned
            site = selected_sites[0]

        first_try = False

    # find the site
    if (site['cname'] == old_cname and site['state']=="in_use"):

        # prompt for cname
        site['cname'] = raw_input("Enter a new cname for this site >> ")

        # record the existing version for later comparison
        old_version = site['version']

        # prompt for a new version
        site['version'] = version_wizard(site, params)

        if old_cname != site['cname']:

            # if cname is changed, change site state to "setup"
            # to trigger a DB reset
            site['state'] = "setup"

        elif ( old_cname == site['cname'] and
               is_first_arg_latest(old_version, site['version'] ) ):

            # site name is unchanged and version requested is older than the current version. This change
            # requires a DB reset. Prompt user to confirm that a reset is OK.
            reset_site = site_change_prompter("The version requested will require a DB reset. OK to reset? (y/n/<enter>) >> ")

            if reset_site:
                # trigger a reset
                site['state'] = "setup"
            else:
                # keep the site at its existing version
                print "The %s site will be left as is"%site['cname']
                site['version'] = old_version

    return site

def version_wizard(site, params):

    gold_image_version = get_variable(params,'gold-image-version')

    # prompt for version
    msg = ("Enter Confluence version for this site\n" +
           "Note that version must be %s or greater\n" +
           "Enter version >> ")%gold_image_version
    
    version = raw_input(msg)

    while not ( is_a_version(version) and
                       (is_same(version, gold_image_version) or
                        is_first_arg_latest(version, gold_image_version)) ):

        # print "version: %s, valid?: %s, latest?: %s"%(version, is_a_version(version), is_first_arg_latest(version, gold_image_version))

        if not is_a_version(version):
            msg = ("Version entered is not a valid version (i.e. X.Y.Z, etc)\n" +
                   "Please enter a valid version >> ")

        elif not is_first_arg_latest(version, gold_image_version):
            msg = ("Version entered was not released later than 5.8.9 (i.e. X.Y.Z etc., where X/Y/Z are integers)\n" +
                   "Please enter a more recent version >> ")

        version = raw_input(msg)

    # see if image is available
    if not image_exists(params, "nordstromsets/confluence", version):

        # if we are running against a local servicefile, use the local dockerfile as well
        servicefile_path = get_variable(params,'servicefile-path')

        if servicefile_path:

            build_path = os.path.join(servicefile_path, 'docker')

        else:

            # download the dockerfile from the demos repo
            repo_url = "https://bitbucket.org/brikitaws/demos-stack/get/master.zip"

            # download dockerfile sources to ~/.yac/<image name>/.
            build_path = download_dockerfile("nordstromsets/confluence", repo_url)

        # get stack name
        stack_name = get_stack_name(params)

        # build image to target env
        build_confluence_image(stack_name, version, build_path)

        # prompt user to suggest need to push image to repo
        raw_input( ("If image built successfully push to repo after testing image.\n" +
                    "Press <enter> to continue >> ") )

    return version

def site_change_prompter(msg):

    validation_failed = True
    change = False

    # accept y, n, or empty string
    options = ['y','n', '']

    while validation_failed:

        input = raw_input(msg)

        # validate the input
        validation_failed = string_validation(input, options, False)

    # change iff 'y' was input
    if (input and input == 'y'):
        change = True

    return change

def image_exists(params, image_name, version):

    # check versions in repo

    hub_versions = get_image_versions(image_name)

    image_exists = version in hub_versions

    if not image_exists:

        # check local images

        # get stack name
        stack_name = get_stack_name(params)

        # get the ip address of the target host
        target_host_ip = get_ec2_ips( stack_name , "", True)[0]

        docker_api_conn_str = get_connection_str( target_host_ip )

        local_versions = get_image_versions_local(docker_api_conn_str, image_name)

        image_exists = version in local_versions

    return image_exists

def build_confluence_image(stack_name, version, build_path):

    image_tag = "%s:%s"%("nordstromsets/confluence",version)

    # get the ip address of the target host
    target_host_ips = get_ec2_ips( stack_name , "", True)

    user_msg =  "%sThe %s container image does not yet exist on the %s stack%s"%('\033[92m',
                                            image_tag,
                                            stack_name,                                                    
                                            '\033[0m')

    raw_input(user_msg + "\nHit <enter> to build image using dockerfile at %s..."%build_path )

    # build the container image on each host ...
    for target_host_ip in target_host_ips:

        # get connection string for the docker remote api on the target host
        docker_api_conn_str = get_connection_str( target_host_ip )

        # add confluence version as a build arg
        buildargs = {'CONFLUENCE_VERSION': version}

        # build the image
        build_image( image_tag, build_path, docker_api_conn_str, False, buildargs )

def load_sites(params):

    sites = []
    s3_path = get_state_s3_path(params)
    service_alias = get_variable(params,'service-alias')
    s3_bucket = get_variable(params,'s3-bucket')

    demos_state = load_state_s3(s3_bucket,s3_path,service_alias)

    # save the all the state params into a single variable so they 
    # can easily be re-saved if sites are altered
    set_variable(params, 'demos-state', demos_state, "state of demos sites")

    # save the state variables to params for easy individual recall
    params.update(demos_state)

    # pull out the sites
    sites = get_variable(demos_state,'sites')

    return sites

def save_sites(params, sites):

    demo_state = get_variable(params, 'demos-state')

    set_variable(demo_state,'sites',sites)

    s3_path = get_state_s3_path(params)
    service_alias = get_variable(params,'service-alias')
    s3_bucket = get_variable(params,'s3-bucket')

    save_state_s3(s3_bucket, s3_path, service_alias, demo_state)

def get_state_s3_path(params):

    service_alias = get_variable(params,'service-alias',"")
    env = get_variable(params,'env',"")

    return "%s/%s"%(service_alias,env)

def pp_site_list(list):

    str = ""
    for item in list:
        str = str + '* %s@%s\n'%(item['cname'],item['version'])

    return str