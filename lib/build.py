import re, boto3, os, json, jmespath, sys, argparse
from os.path import expanduser
from urlparse import urlparse
from yac.lib.inputs import get_inputs, get_stack_service_inputs_value, pp_list
from yac.lib.inputs import get_image_versions, string_validation
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists, get_ec2_ips
from yac.lib.template import apply_stemplate
from yac.lib.container.api import get_connection_str
from yac.lib.container.build import build_image, get_image_name, get_image_versions_local
from yac.lib.container.build import download_dockerfile
from yac.lib.state import load_state_s3, save_state_s3
from yac.lib.version import is_first_arg_latest, is_a_version, is_same
from yac.lib.task import handle_task_inputs

def task_handler(params, stack_template, inputs, conditional_inputs):

    print "task <servicefile>/rebuild-image starting ..."

    # get inputs associated with this task
    task_params = handle_task_inputs(inputs,conditional_inputs)

    # get the site input
    confluence_version = get_variable(task_params,'confluence-version')
    dockerfile_path = get_variable(task_params,'dockerfile-path')

    # build the docker image
    gold_image_name = build(params, confluence_version, dockerfile_path)

    print "task <servicefile>/rebuild-image complete"

def build(params, confluence_version, dockerfile_path):

    while not ( is_a_version(confluence_version) ):

        if not is_a_version(confluence_version):
            msg = ("Version entered is not a valid version (i.e. X.Y.Z, etc)\n" +
                   "Please enter a valid version >> ")

        confluence_version = raw_input(msg)

    if not dockerfile_path:

        # get dockerfile from the repo
        repo_url = "https://bitbucket.org/brikitaws/demos-stack/get/master.zip"
        # download to ~/.yac/<image name>/.
        dockerfile_path = download_dockerfile("nordstromsets/confluence", repo_url)

    # get stack name
    stack_name = get_stack_name(params)

    # build image to target env
    build_confluence_image(stack_name, confluence_version, dockerfile_path)

    return confluence_version

def build_confluence_image(stack_name, confluence_version, build_path):

    image_tag = "%s:%s"%("nordstromsets/confluence",confluence_version)

    # get the ip address of the target host
    target_host_ips = get_ec2_ips( stack_name , "", True)

    user_msg =  "%sBuilding the %s container image to the %s host using dockerfile at %s.%s"%('\033[92m',
                                            image_tag,
                                            stack_name,    
                                            build_path,                                                
                                            '\033[0m')

    raw_input(user_msg + "\nHit <enter> to build image ..." )

    # build the container image on each host ...
    for target_host_ip in target_host_ips:

        # get connection string for the docker remote api on the target host
        docker_api_conn_str = get_connection_str( target_host_ip )

        # add confluence version as a build arg
        buildargs = {'CONFLUENCE_VERSION': confluence_version}

        # build the image
        build_image( image_tag, build_path, docker_api_conn_str, False, buildargs )
