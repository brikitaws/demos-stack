import json, datetime, os, jmespath
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists, get_ec2_ips, cp_file_to_s3
from yac.lib.stack import get_rds_endpoints
from yac.lib.stack import stop_service_blocking, start_service
from yac.lib.template import apply_stemplate
from yac.lib.db import DB, get_connection_string
from yac.lib.db import get_table, create_db, db_exists, drop_db
from yac.lib.container.config import get_aliases, env_ec2_to_api, get_container_volumes
from yac.lib.container.config import get_container_names
from yac.lib.container.config import find_image_tag, get_container_ports, get_container_envs
from yac.lib.container.start import start, execute
from yac.lib.container.api import get_connection_str, find_container_by_name
from yac.lib.file import dump_dictionary
from yac.lib.task import handle_task_inputs
from yac.lib.cache import get_cache_value, set_cache_value_dt

SECRETS_CACHE_KEY = "brikit-demos:db-secrets"

def task_handler(params, stack_template, inputs, conditional_inputs):

    print "task <servicefile>/gold-image starting ..."

    sites = get_variable(params,'sites',[])
    in_use = jmespath.search("[?state=='in_use']",sites)

    print "\nThe following demo sites are currently available for use as templates:\n%s"%pp_site_list(in_use)

    # get the cnames of sites currently in use. these are the gold image candidates
    cnames = jmespath.search("[?state=='in_use'].cname",sites)

    # add the cnames as input options
    inputs["gold-site"]["constraints"]["options"] = cnames

    # get inputs associated with this task
    task_params = handle_task_inputs(inputs,conditional_inputs)

    # get the site input
    gold_site_cname = get_variable(task_params,'gold-site')

    gold_site = jmespath.search("[?cname=='%s']"%gold_site_cname,sites)[0]

    # stop the Confluence services associated with gold_site
    stop_confluence_service(params, gold_site)

    # setup the gold image
    gold_image_name = setup_gold_image(params , gold_site)

    # restart the Confluence services associated with gold_site
    start_confluence_service(params, gold_site)

    # update site state to reflect the new template
    save_demos_state(params, gold_image_name, gold_site, task_params)

    print "task <servicefile>/gold-image complete"

def add_version_to_name(site_names,site_version):

    site_name_and_version = []

    for i,name in enumerate(site_names):
        site_name_and_version = site_name_and_version + ["%s@%s"%(name,site_version[i])]

    return site_name_and_version

def setup_gold_image(params , gold_site):

    gold_image_name = ""

    # get the stack name
    stack_name = get_stack_name(params)

    # get the endpoint address of the db for this stack
    rds_endpoints = get_rds_endpoints(stack_name)

    if len(rds_endpoints)==1:

        rds_endpoint = rds_endpoints[0]
        
        sysadmin_user = get_variable(params, "rds-master-user")     

        # get the sites
        sites = get_variable(params, "sites") 

        # get db user from params
        app_db_user_name = get_variable( params, 'db-user' )

        # get password
        sysadmin_pwd = get_master_password()

        if sysadmin_pwd:
            
            db_engine = get_variable( params, 'rds-engine' )
            db_port = get_variable( params, 'db-port' )

            # get the db configs for this env
            db = DB('postgres',
                     sysadmin_user,
                     sysadmin_pwd,
                     rds_endpoint['Address'],
                     db_port,
                     db_engine)

            # setup the DB
            gold_image_name = create_gold_image(db, gold_site, params)

    else:

        # break out of the scheduled loop
        print 'Could not find RDS endpoint for the %s stack. Gold image creation failed.'%(stack_name)

    return gold_image_name

# create a template DB (aka 'gold image')
def create_gold_image(db, gold_site, params):

    # db name of gold site
    slot_db_name = "slot%s"%gold_site['slot']

    raw_input( ("About to create the gold DB image based on the %s site. "%gold_site['cname'] +
                "Press <enter> to continue ... >> ") )

    # db name for old confluence version
    # naming convension is confluence_<version>, with periods in version 
    # replaced with underscores
    new_gold_name = "confluence_%s"%gold_site['version'].replace('.','_')

    # if a same-name gold image already exists
    if db_exists(db, new_gold_name):

        # drop the existing gold image
        drop_db_sql = "DROP DATABASE %s;"%new_gold_name
        drop_db(db, drop_db_sql )
        
    # get the version associated with the old gold image
    demo_state = get_variable(params, 'demos-state')
    old_gold_version = get_variable(demo_state,'gold-image-version')
    old_gold_name = get_variable(demo_state,'gold-image-db_name')

    # create the "template" DB from the site template
    create_db_sql = "CREATE DATABASE %s TEMPLATE %s;"%(new_gold_name,slot_db_name)
    create_db(db, create_db_sql )

    # drop the old gold image
    if (new_gold_name != old_gold_name and 
        db_exists(db, old_gold_name) ):

        # drop the existing db
        drop_db_sql = "DROP DATABASE %s;"%old_gold_name
        drop_db(db, drop_db_sql )
        
    return new_gold_name

def save_demos_state(params, gold_image_name, gold_site, task_params):

    if gold_image_name:

        demo_state = get_variable(params, 'demos-state')

        gold_image_version = gold_site['version']
        gold_image_build_number = get_variable(task_params, 'build-number')

        # save the gold image state to the site state
        set_variable(demo_state,'gold-image-version',gold_image_version,
                    "the version of Confluence cooked into the template DB")
        set_variable(demo_state,'gold-image-build-number',gold_image_build_number,
                    "The Confluence build number associated with the template DB")
        set_variable(demo_state,'gold-image-db_name',gold_image_name,
                    "the name of the DB that serves as the 'gold image' template for new sites")

        s3_bucket = get_variable(params,'s3-bucket')
        service_alias = get_variable(params,'service-alias',"")
        s3_path = get_state_s3_path(params)

        # save state to s3
        save_state_s3(s3_bucket, s3_path, service_alias, demo_state)


def get_state_s3_path(params):

    service_alias = get_variable(params,'service-alias',"")
    env = get_variable(params,'env',"")

    return "%s/%s"%(service_alias,env)

def get_master_password():

    # see if the db userpwd is in the user's local secrets cache
    db_secrets = get_cache_value(SECRETS_CACHE_KEY, {})

    sysadmin_pwd = ""

    if ('rds-master-pwd' not in db_secrets):

        msg = "Please enter the password for the DB sysadmin user >> "
        sysadmin_pwd = raw_input(msg)

        # save pwd back to the secrets cache for future reference
        # with max timeout
        db_secrets['rds-master-pwd'] = sysadmin_pwd
        set_cache_value_dt(SECRETS_CACHE_KEY,
                           db_secrets)

    else:
        sysadmin_pwd = db_secrets['rds-master-pwd']

    return sysadmin_pwd

def stop_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)

    print "Stopping Confluence on the %s site to prepare for the DB template creation ..."%site['cname']

    # stop service, and block until the service is truly stopped
    stop_service_blocking(stack_name,name_search_string)  

def start_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)
    print "Re-starting Confluence for the %s site ..."%site['cname']

    start_service(stack_name,name_search_string)

def pp_site_list(list):

    str = ""
    for item in list:
        str = str + '* %s@%s\n'%(item['cname'],item['version'])

    return str    