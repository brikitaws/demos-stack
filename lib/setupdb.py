#!/usr/bin/env python

import time, os, subprocess, json, sys, boto3
import socket, getpass
import datetime as dt
from colorama import Fore, Style
from sqlalchemy import *
from sqlalchemy import create_engine

from yac.lib.stack import get_stack_name, get_stack_state, cp_file_to_s3
from yac.lib.stack import NEWLY_CREATED_STATE, CREATE_IN_PROGRESS_STATE 
from yac.lib.stack import UPDATE_COMPLETE_STATE,UPDATE_IN_PROGRESS_STATE
from yac.lib.stack import UPDATE_COMPLETE_STATES, get_stack_elbs
from yac.lib.stack import stop_service_blocking, start_service
from yac.lib.stack import get_rds_endpoints, get_ec2_ips, get_ecs_service
from yac.lib.variables import get_variable, set_variable
from yac.lib.state import save_state_s3
from yac.lib.inputs import get_stack_service_inputs
from yac.lib.service import get_service
from yac.lib.db import DB, get_connection_string
from yac.lib.db import get_table, create_db, db_exists, drop_db
from yac.lib.template import apply_stemplate, apply_templates_in_file
from yac.lib.file import dump_dictionary, localize_file, get_dump_path
from yac.lib.container.api import get_connection_str, find_container_by_name
from yac.lib.container.start import execute
from yac.lib.cache import get_cache_value, set_cache_value_dt

SECRETS_CACHE_KEY = "brikit-demos:db-secrets"

# called after run yac stack.
# performs post-processing needed to make the stack usable.
def get_value(params):

    print "starting <servicefile>/setupdb.py ..."

    # get the name of the stack that corresponds to this app and env
    stack_name = get_stack_name(params)

    timer_start=dt.datetime.now()

    rds_endpoint = get_rds_endpoints(stack_name)[0]

    # wait for the status of the rds endpoint to change to "available"
    while ( 'available' not in rds_endpoint['Status'] ):

        now=dt.datetime.now()
        elapsed_secs = (now-timer_start).seconds
        elapsed_mins = elapsed_secs/60
        sys.stdout.write('\r')
        msg = ("After %s minutes, %s RDS changes are in progress so DB updates are still pending. " +
               "Current RDS state is: %s ... ")%(elapsed_mins,stack_name,rds_endpoint['Status'])
        sys.stdout.write(msg)
        sys.stdout.flush()

        # sleep for 30 seconds then check stack state again
        time.sleep(30)

        # check the state of the endpoint
        rds_endpoint = get_rds_endpoints(stack_name)[0]

    print "\nRDS service in %s stack is available ... checking if DB updates are necessary ..."%stack_name  

    setup_dbs(params, stack_name, timer_start)

    print "\nDB updates are complete"  


def setup_dbs(params , stack_name, timer_start):

    # get the endpoint address of the db for this stack
    rds_endpoints = get_rds_endpoints(stack_name)

    if len(rds_endpoints)==1:

        rds_endpoint = rds_endpoints[0]
        
        sysadmin_user = get_variable(params, "rds-master-user")     

        # get the sites
        sites = get_variable(params, "sites") 

        # get db user from params
        app_db_user_name = get_variable( params, 'db-user' )

        # get the name of the db template from params
        gold_image_db_name = get_variable(params, 'gold-image-db_name')

        # get password
        sysadmin_pwd = get_master_password()

        if sysadmin_pwd:
            
            db_engine = get_variable( params, 'rds-engine' )
            db_port = get_variable( params, 'db-port' )

            # get the db configs for this env
            db = DB('postgres',
                     sysadmin_user,
                     sysadmin_pwd,
                     rds_endpoint['Address'],
                     db_port,
                     db_engine)

            # setup DBs in any site in the 'setup' state
            sites_updated = False

            for site in sites:

                if 'setup' in site['state']:
                # if site['slot'] == 5:

                    sites_updated = True

                    # name DBs to match the slot name
                    db_name = "slot%s"%site['slot']

                    raw_input( ("About to setup the DB for the %s site. "%site['cname'] +
                                "Press <enter> to continue ... >> ") )

                    # stop the Confluence services associated with this site
                    stop_confluence_service(params, site)

                    # setup the DB
                    setup_db(db, db_name, gold_image_db_name)

                    print "%s DB created using the %s 'gold' image"%(site['cname'],gold_image_db_name)

                    # copy a confluence.cfg.xml file to the site that is suitable
                    # for a freshly setup site
                    setup_db_config_file(params, site, db)

                    # re-start Confluence
                    start_confluence_service(params, site)

                    # update state
                    site['state'] = "in_use"

                    print site_instructions(params, site)

            # setup the db user/role that Confluence will connect as
            app_db_user_pwd = setup_db_user(db,app_db_user_name)

            if sites_updated:

                # save the updates sites to s3
                save_sites(params, sites)

    else:

        # break out of the scheduled loop
        print 'Could not find RDS endpoint for the %s stack. DB setup skipped.'%(stack_name)

def save_sites(params, sites):

    demo_state = get_variable(params, 'demos-state')

    set_variable(demo_state,'sites',sites)

    s3_path = get_state_s3_path(params)
    service_alias = get_variable(params,'service-alias')
    s3_bucket = get_variable(params,'s3-bucket')
    
    save_state_s3(s3_bucket, s3_path, service_alias, demo_state)

def get_db_urls(rds_endpoint, db_port, db_names):

    urls = ""
    for db_name in db_names:
        urls = urls+"- jdbc:postgresql://%s:%s/%s\n"%(rds_endpoint,db_port,db_name)

    return urls

def get_master_password():

    # see if the db userpwd is in the user's local secrets cache
    db_secrets = get_cache_value(SECRETS_CACHE_KEY, {})

    sysadmin_pwd = ""

    if ('rds-master-pwd' not in db_secrets):

        msg = "Please enter the password for the DB sysadmin user >> "
        sysadmin_pwd = raw_input(msg)

        # save pwd back to the secrets cache for future reference
        # with max timeout
        db_secrets['rds-master-pwd'] = sysadmin_pwd
        set_cache_value_dt(SECRETS_CACHE_KEY,
                           db_secrets)

    else:
        sysadmin_pwd = db_secrets['rds-master-pwd']

    return sysadmin_pwd

def get_app_user_password(db_username):

    # see if the db userpwd is in the user's local secrets cache
    db_secrets = get_cache_value(SECRETS_CACHE_KEY, {})

    db_userpwd = ""

    if ('db-userpwd' not in db_secrets):

        msg = ("Confluence will connect to its Postgres DB with " +
               "the username '%s'. Please provide a password for " +
               "the '%s' user. >> ")%(db_username,db_username)

        db_userpwd = raw_input( msg )

        # save pwd back to the secrets cache for future reference
        # with max timeout
        db_secrets['db-userpwd'] = db_userpwd
        set_cache_value_dt(SECRETS_CACHE_KEY,
                           db_secrets)

    else:
        db_userpwd = db_secrets['db-userpwd']

    return db_userpwd

def site_instructions(params, site):

    stack_name = get_stack_name(params)

    # get the ELB associated with this site
    elb = get_stack_elbs(stack_name, "slot%s"%site['slot'])

    fqdn = get_fqdn(params, site['cname'])

    msg = ('The https://%s demo site should be available for use in a few minutes\n'%fqdn +
           '(pending the normal Confluence warmup/upgrade time)\n' + 
           'The %s cname should alias to the following elb aname: %s\n'%(site['cname'],elb['DNSName']) +
           'After the cname starts resolving, you can access your site.\n')

    return msg

def get_fqdn(params, cname):

    tl_domain = get_variable(params,'tl-domain')
    sl_domain = get_variable(params,'sl-domain')

    return "%s.%s.%s"%(cname,sl_domain,tl_domain)

# create a postgres db and a local user for jira
def setup_db(db, db_name, gold_image_db_name):

    if db_exists(db, db_name):

        # drop the existing db
        drop_db_sql = "DROP DATABASE %s;"%db_name
        drop_db(db, drop_db_sql )

    # create the DB from the "gold-standard" template, which contains
    # an initialized Confluence schema and brikit plugins
    create_db_sql = "CREATE DATABASE %s TEMPLATE %s;"%(db_name,gold_image_db_name)
    create_db(db, create_db_sql )

# create a local db user for confluence
def setup_db_user(db, db_username):

    # create the role if it does not already exist
    if not role_exists(db, db_username):

        db_userpwd = get_app_user_password(db_username)

        connection_str = get_connection_string(db)

        engine = create_engine(connection_str)
        
        conn = engine.connect()

        # execute the create role command
        conn.execute("CREATE ROLE %s WITH CREATEROLE CREATEDB LOGIN;"%db_username)

        # and set pwd for the role
        conn.execute("ALTER ROLE %s WITH PASSWORD '%s'"%(db_username, db_userpwd))

        conn.close()     

def role_exists(db, role_name):

    role_exists=False

    roles_table = get_table('pg_roles', db)

    stmt = select([roles_table.c.oid]).\
           where(roles_table.c.rolname==role_name)

    results = stmt.execute()

    if results.fetchone():
        role_exists = True

    return role_exists

def stop_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)

    print "Stopping Confluence for the %s site to prepare for DB setup ..."%site['cname']

    # stop service, and block until the service is truly stopped
    stop_service_blocking(stack_name,name_search_string)  

def start_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)
    print "Re-starting Confluence for %s site ..."%site['cname']

    start_service(stack_name,name_search_string)


def setup_db_config_file(params, site, db):

    print "setting up db config file ..."

    # deploy the confluence.cfg.xml file to S3
    deploy_config_file(params, site, db)

    # create the backup container configuration file that will instruct
    # the backups container on how to restore the confluence.cfg.xml to the host
    local_file_path = create_restore_config_file(params, site)

    # deploy the restore config file to s3
    restore_config_file_s3_url = push_restore_config_file(params,
                                                          site,
                                                          local_file_path)

    # execute the restore that will pull the confluence.cfg.xml from S3
    # to the host
    execute_restore(params, restore_config_file_s3_url)

def deploy_config_file(params, site, db):

    servicefile_path = get_variable(params,"servicefile-path")

    # path to the templatized version of confluence.cfg.xml
    template_file_path = get_variable(params,"confluence-cfg-xml-tmpl")

    s3_destination = get_file_destination(params, site['slot'])

    # localize the template file
    localized_file = localize_file(template_file_path, servicefile_path)

    # render the file into the local 'dump' directory
    service_name = get_variable(params,"service-name","unknown")

    rendered_file_path = get_dump_path(service_name)

    if os.path.exists(localized_file):

        # prompt the user to input a fresh evaluation license for this site
        eval_license_str = prompt_for_license()

        db_username = get_variable(params,"db-user")
        build_no = get_variable(params,'gold-image-build-number')

        # create a params dict containting the variables that need to be rendered
        # into the config/confluence.cfg.xml file, including: the license, rds endpoint, 
        # db name, db user, db user pwd, and the confluence build number
        cfg_file_variables = {}
        set_variable(cfg_file_variables,"db-userpwd", get_app_user_password(db_username))        
        set_variable(cfg_file_variables,"rds-endpoint", db.configs['db_host'])
        set_variable(cfg_file_variables,"db-port", db.configs['db_port'])
        set_variable(cfg_file_variables,"db-name", "slot%s"%site['slot'])
        set_variable(cfg_file_variables,"db-user", db_username)
        set_variable(cfg_file_variables,"build-number", build_no)
        set_variable(cfg_file_variables,"eval-license", eval_license_str)

        # print "db-params: %s"%json.dumps(cfg_file_variables,indent=2)

        # replace any service parmeters variables in the file body and return the 
        # name+path of the "rendered" file
        rendered_file = apply_templates_in_file(localized_file, 
                                                cfg_file_variables, 
                                                rendered_file_path)

        print("Copying rendered confluence.cfg.xml to S3 ...")

        # copy rendered file to s3 destination 
        cp_file_to_s3( rendered_file, s3_destination)

    else:
        print("Error: could not find localized version of confluence.cfg.xml for rendering")

def prompt_for_license():

    license = ""
    print('Please provide an evaluation license for this site (copy/paste from my.atlassian then press <enter> twice to submit) >> ')
    while True:

        this_line = raw_input()
        
        if not this_line:            
            break

        license = license + this_line

    return license

def execute_restore(params, restore_config_file_s3_url):

    # get stack name
    stack_name = get_stack_name(params)

    # get the ip address of the target host
    target_host_ip = get_ec2_ips( stack_name ,"", True)[0]

    # get connection string for the docker remote api on the target host
    docker_api_conn_str = get_connection_str( target_host_ip )

    container_desc = find_container_by_name( "backups", docker_api_conn_str)

    # print json.dumps(container_desc,indent=2)

    if "Names" in container_desc and len(container_desc["Names"])==1:

        # docker py sometimes as a '/' char in the prefix of the name
        container_name = container_desc["Names"][0].strip("/")

        # use the restore command per the backups container readme
        # python -m src.restore <configs file url> <hostname>
        service_alias = get_variable(params,'service-alias')
        env = get_variable(params,'env')
        host_name = "%s-%s"%(service_alias,env)

        container_cmd = "python -m src.restore %s %s"%(restore_config_file_s3_url,host_name)

        user_msg = "Restoring the backup file to the host using the %s container on %s via command: \n%s"%(container_name,target_host_ip,container_cmd)

        # execute the command on the backups container
        execute(
            container_name=container_name,
            exec_cmd=container_cmd,
            connection_str=docker_api_conn_str)

    elif "Names" in container_desc and len(container_desc["Names"])==1:
        print "Error: could not find a unique container to run restore against. Found: %s"%container_desc["Names"]
    elif "Names" not in container_desc:
        print "Error: could not find backups container. Restore failed"


def get_file_destination(params, slot_no):

    s3_bucket = get_variable(params,'s3-bucket')
    service_alias = get_variable(params, 'service-alias')
    env = get_variable(params,'env')

    return "s3://%s/%s/%s/home/%s/confluence.cfg.xml"%(s3_bucket,service_alias,env,slot_no)

# push the restore config file to s3
def create_restore_config_file(params, site):

    src = "/var/local/atlassian/confluence/%s/confluence.cfg.xml"%site['slot']
    destination = get_file_destination(params, site['slot'])

    # create the config dictionary formatted per the backups container (see associated Dockerfile README)
    backup_configs = {
        "comment": ["A config suitable for a restore of the confluence.cfg.xml file.",
                    "File created via demos-stack/lib/setupdb.create_restore_config_file()"],
        "daily-times": ["7:00"],
        "files": {
            "confluence.cfg.xml": {
                "comment": ["Restore this file",
                            "Recall that restore works the reverse of backups, so the 'src' becomes"
                            " the destination in a restore use case"],
                "src":  src,
                "dest": destination
            }
        }
    }

    service_name = get_variable(params,'service-name')

    # dump the configs to a local file
    local_file = dump_dictionary(backup_configs, 
                                 service_name, 
                                 "backup-configs.json")

    return local_file
 

def push_restore_config_file(params,site,local_file):

    # give it an arbitrary filename, but one that will make some sense
    # when viewed in s3
    backup_config_file_name = "db-conn-setup.json"

    s3_bucket = get_variable(params,'s3-bucket')
    service_alias = get_variable(params, 'service-alias')
    env = get_variable(params,'env')

    backup_config_file_url = "s3://%s/%s/%s/%s"%(s3_bucket,service_alias,env,backup_config_file_name)

    # cp the file to s3
    cp_file_to_s3( local_file, backup_config_file_url)

    return backup_config_file_url

