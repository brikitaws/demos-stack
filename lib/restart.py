import json, datetime, os, jmespath
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists, get_ec2_ips, cp_file_to_s3
from yac.lib.stack import get_rds_endpoints
from yac.lib.stack import stop_service_blocking, start_service
from yac.lib.template import apply_stemplate
from yac.lib.db import DB, get_connection_string
from yac.lib.db import get_table, create_db, db_exists, drop_db
from yac.lib.container.config import get_aliases, env_ec2_to_api, get_container_volumes
from yac.lib.container.config import get_container_names
from yac.lib.container.config import find_image_tag, get_container_ports, get_container_envs
from yac.lib.container.start import start, execute
from yac.lib.container.api import get_connection_str, find_container_by_name
from yac.lib.file import dump_dictionary
from yac.lib.task import handle_task_inputs
from yac.lib.cache import get_cache_value, set_cache_value_dt

def task_handler(params, stack_template, inputs, conditional_inputs):

    print "task <servicefile>/gold-image starting ..."

    sites = get_variable(params,'sites',[])
    in_use = jmespath.search("[?state=='in_use']",sites)

    print "\nThe following demo sites are currently available for a restart:\n%s"%pp_site_list(in_use)

    # get the cnames of sites currently in use. these are the gold image candidates
    cnames = jmespath.search("[?state=='in_use'].cname",sites)

    # get inputs associated with this task
    task_params = handle_task_inputs(inputs,conditional_inputs)

    # get the site input
    restart_cname = get_variable(task_params,'restart-site')

    restart_site = jmespath.search("[?cname=='%s']"%restart_cname,sites)[0]

    # stop the Confluence service for the site input
    stop_confluence_service(params, restart_site)

    # restart the Confluence services for the site input
    start_confluence_service(params, restart_site)

    print "task <servicefile>/restart complete"

def stop_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)

    print "Stopping Confluence on the %s site ..."%site['cname']

    # stop service, and block until the service is truly stopped
    stop_service_blocking(stack_name,name_search_string)  

def start_confluence_service(params, site):

    name_search_string = "Slot%s"%site['slot']
    stack_name = get_stack_name(params)
    print "Re-starting Confluence for the %s site ..."%site['cname']

    start_service(stack_name,name_search_string)

def pp_site_list(list):

    str = ""
    for item in list:
        str = str + '* %s@%s\n'%(item['cname'],item['version'])

    return str    