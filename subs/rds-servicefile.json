{
  "service-name": {
    "comment": "the name for this service in the yac registry",
    "value": "nordstromsets/rds:1.0"
  },  
  "default-alias": {
    "comment": "the default alias to assign this service (used for resource naming, can be overridded in 'yac stack')",
    "value": "rds"
  },      
  "service-description": {
    "summary": "Relational Database Service, typically providing a service to other stacks.",
    "details":  ["* Scalable Storage",
                 "* Failover to multiple AZ",
                 "* Auto-backups",
                 "* Intended for use with nordstromsets/asg service"],
    "maintainer" : {
      "name":     "Thomas Jackson",
      "email":    "thomas.b.jackson@gmail.com"
    },
    "tags": ["efs"]
  },
  "stack-template": {
    "Parameters" : {
      "MasterUserPassword" : {
        "Description" : "The password of the sys-admin user for the RDS instance.",
        "Type" : "String",
        "NoEcho": true
      },      
      "DBSnapshotIdentifier" : {
        "Description" : "The ID of the RDS snapshot to build the DB from.",
        "Type" : "String"      
      }      
    },
    "Resources" : {
      "DB" : {
        "Type" : "AWS::RDS::DBInstance",
        "Properties" : {
          "VPCSecurityGroups": [{"Ref" : "DBSG"}],
          "AllocatedStorage" : {"yac-ref": "rds-size"},
          "Engine" : {"yac-ref": "rds-engine"},
          "StorageType": "gp2",
          "BackupRetentionPeriod": {"yac-ref": "rds-backup-retention"},
          "PreferredBackupWindow": "00:00-00:30",
          "MultiAZ": true,
          "DBInstanceClass": {"yac-ref": "rds-instance-type"},
          "DBInstanceIdentifier": { "yac-name" : "db-host" },
          "MasterUsername" :     {"yac-ref":  "rds-master-user"},
          "MasterUserPassword" :  {"Ref":  "MasterUserPassword"},
          "DBSnapshotIdentifier": {"Ref":  "DBSnapshotIdentifier"},
          "PubliclyAccessible" : true,
          "Tags" : [ 
            {"Key": "Name", "Value" : { "yac-name" : "db-host" } },
            { "Key": "Owner", "Value": { "yac-ref": "owner" } }          
          ],
          "DBSubnetGroupName": {"yac-ref" : "db-subnet-group-name"}
        }
      },
      "DBSG" : {
        "Type" : "AWS::EC2::SecurityGroup",
        "Properties" : {
          "GroupDescription" : "Enable admin access from domain to DBs",
          "VpcId": {"yac-ref" : "vpc-id"},
          "Tags" : [ 
            { "Key": "Name", "Value" : {  "yac-name": "db-sg"}},
            { "Key": "Owner", "Value": { "yac-ref": "owner" } }
          ],
          "SecurityGroupIngress" : [
          {
            "IpProtocol" : "tcp",
            "FromPort" : {"yac-ref": "db-port"},
            "ToPort" : {"yac-ref": "db-port"},
            "CidrIp" : "0.0.0.0/0"
          }]
        }
      }, 
      "DBAppIngress" : {
        "Type" : "AWS::EC2::SecurityGroupIngress",
        "Properties" : {
          "GroupId" : { "Fn::GetAtt": ["DBSG","GroupId"]},
          "IpProtocol" : "tcp",
          "ToPort" : {"yac-ref": "db-port"},
          "FromPort" : {"yac-ref": "db-port"},
          "SourceSecurityGroupId" : {"Fn::GetAtt": ["AppSG","GroupId"]}
        }
      },
      "AppDBEgress" : {
        "Type" : "AWS::EC2::SecurityGroupEgress",
        "Properties" : {
          "GroupId" : { "Fn::GetAtt": ["AppSG","GroupId"]},
          "IpProtocol" : "tcp",
          "ToPort" : {"yac-ref": "db-port"},
          "FromPort" : {"yac-ref": "db-port"},
          "DestinationSecurityGroupId" : {"Fn::GetAtt": ["DBSG","GroupId"]}
        }
      }
    }
  },
  "service-inputs": {
    "comments": ["Prompt user for these values."],
    "inputs": {                    
      "vpc-id": {
        "description": "VPC ID",
        "help":    "The ID of the VPC to build stack in",      
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true,
          "options_fxn": {
            "name": "get_vpc_ids"
          }
        }
      },                                                  
      "owner":  {
        "description": "Owner",
        "help":    "The stack owner (typically distribution list). Used for notifications",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },
      "rds-size":  {
        "description": "RDS Size",
        "help":    "The size of the RDS DB that provides the Jira DB (in GB).",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      },
      "rds-master-user":  {
        "description": "RDS Master User",
        "help":    "The name of the sys-admin user for the RDS instance.",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },           
      "db-port":  {
        "description": "DB Port",
        "help":    "The port the DB should listen on",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      },              
      "rds-backup-retention":  {
        "description": "RDS Backup Retention",
        "help":    "The depth of RDS snapshots maintained, in days",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      },
      "rds-engine":  {
        "description": "RDS Engine",
        "help":    "The DB engine to use (see AWS RDS docs for available setpoints)",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      }           
    },
    "conditional-inputs": {   
      "db-subnet-group-name":  {
        "description": "DB Subnet Group Name",
        "help": ["Name of for DB subnet group  in this VPC."],
        "comment": ["Can only be set once VPC ID is known",
                    "Name can alternatively be set via vpc prefs or static params."],
        "wizard_fxn":  {
          "name": "array_wizard",
          "arg":  "private"
        },
        "constraints": {
          "required": false,
          "options_fxn": {
            "name": "get_db_subnet_group_ids"
          }
        },
        "condition": {
          "input": "vpc-id"
        }                  
      },    
      "rds-master-pwd":  {
        "description": "RDS Master Password",
        "help":    "The password of the sys-admin user for the RDS instance.",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        },
        "condition": {
          "fxn":   "does_stack_exist",
          "value": false
        },
        "param": {
          "comment": ["Copy this input value into a template parameter.",
                      "Only provide a value at stack creation (since pwd can't be changed on an existing instance)"],
          "key": "MasterUserPassword",
          "on-updates": false
        }  
      },
      "rds-snapshot-id":  {
        "description": "RDS Snapshot ID",
        "help":    "The ID of the RDS snapshot to build the DB from. ",        
        "comment": ["User should only be prompted for this input for new stacks (not updated stacks) since RDS can",
                    " only initialize from a snapshot for new RDS instances."],
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": false
        },
        "condition": {
          "fxn":   "does_stack_exist",
          "value": false
        },
        "param": {
          "comment": ["Copy this input value into a template parameter.",
                      "Only provide a value at stack creation (since snapshot can't be changed on an existing instance)"],
          "key": "DBSnapshotIdentifier",
          "on-updates": false
        }  
      }                
    }
  }  
}