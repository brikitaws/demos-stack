{
  "service-name": {
    "comment": "the name for this service in the yac registry",
    "value": "nordstromsets/asg:1.0"
  },  
  "default-alias": {
    "comment": "the default alias to assign this service (used for resource naming, can be overridded in 'yac stack')",
    "value": "asg"
  },    
  "service-description": {
    "summary": "Auto-Scaling group, typically providing a service to other stacks.",
    "details":  ["* HA though ASG and ECS",
                 "* Public access via an intranet-facing ELBs"],
    "maintainer" : {
      "name":     "Thomas Jackson",
      "email":    "thomas.b.jackson@gmail.com"
    },
    "tags": ["asg"]
  },
  "stack-template": {      
    "Parameters" : {
      "WebServerPort": {
        "Description" : "Port web server listens on (and auto-redirects to 443)",
        "Type" : "String",
        "Default" : "80"
      },
      "DebugPort": {
        "Description" : "Port web server listens on (and doesn't auto-redirect)",
        "Type" : "String",
        "Default" : "81"
      }
    },
    "Conditions" : {
      "AllowProdAccountIngress" : {"Fn::Equals" : [{"yac-ref" : "env"}, "prod"]},
      "IsDevEnv" : {"Fn::Equals" : [{"yac-ref" : "env"}, "dev"]},
      "IsLowerEnv" : {"Fn::Not" : [{"Fn::Equals" : [{"yac-ref" : "env"}, "prod"]}]}
    },
    "Resources" : {
      "AppAutoScalingGroup": {
        "Type" : "AWS::AutoScaling::AutoScalingGroup",
        "Properties" : {
          "AvailabilityZones": { "yac-ref" : "availability-zones" },
          "LoadBalancerNames": [{ "Ref" : "ExternalElb" },
                                { "Ref" : "ExternalElb2"},
                                { "Ref" : "ExternalElb3"},
                                { "Ref" : "ExternalElb4"},
                                { "Ref" : "ExternalElb5"}],
          "DesiredCapacity" : {"yac-ref": "asg-size"},
          "MaxSize": {"yac-ref": "asg-size"},
          "MinSize": {"yac-ref": "asg-size"},
          "HealthCheckGracePeriod": "600",
          "HealthCheckType": "EC2",
          "LaunchConfigurationName": { "Ref" : "AppLaunchConfig" },
          "VPCZoneIdentifier": {"yac-ref" : "public-subnet-ids"},
          "Tags" : [ 
            { "Key": "Name", "Value" : { "yac-name" : "asg" },"PropagateAtLaunch": true },
            { "Key": "Owner", "Value": { "yac-ref": "owner" },"PropagateAtLaunch": true  }
          ]
        }
      },    
      "AppLaunchConfig" : {
        "Type" : "AWS::AutoScaling::LaunchConfiguration",
        "Properties" : {
          "KeyName" : { "yac-ref" : "ssh-key" },
          "SecurityGroups": [{"Fn::GetAtt": ["AppSG","GroupId"]}],
          "ImageId": { "yac-ref" : "instance-ami" },
          "InstanceType": { "yac-ref": "instance-type" },
          "IamInstanceProfile": {"Ref": "IAMRoleProfile"},
          "AssociatePublicIpAddress": true,
          "BlockDeviceMappings": [
            {
              "DeviceName": "/dev/xvdc",
              "Ebs": {
                "VolumeType": "gp2",
                "DeleteOnTermination": "false",
                "VolumeSize": {"yac-ref" : "docker-vol-size" }
              }
            },
            {
              "DeviceName": "/dev/xvdd",
              "Ebs": {
                "VolumeType": "gp2",
                "DeleteOnTermination": "false",
                "VolumeSize": {"yac-ref" : "home-vol-size" }
              }
            }
          ],
          "UserData" : { "Fn::Base64": { "Fn::Join" : ["", {"yac-fxn": "yac/lib/boot.py"} ] } }
        }
      },
      "IAMRoleProfile": {
        "Type": "AWS::IAM::InstanceProfile",
        "Properties": {
          "Path": {"Fn::Join" : [ "/", [ "",
                      {"yac-join" : [ "/", [
                        {"yac-ref": "service-alias"},
                        {"yac-ref": "env"}]]
                      }, ""] ] },
          "Roles": [{
            "Ref": "IAMRole"
          }]
        }
      },     
      "AppSG" : {
        "Type" : "AWS::EC2::SecurityGroup",
        "Metadata" : {
          "Comment" : "Explanation for each non-standard Ingress port in this SG",
          "Ports" : {
            "5555_Ingress" : "Docker remote API. Need this to be open only in dev env's",
            "80_81_Ingress": "Need both 80 and 81 for http. 81 doesn't redirect so is useful when standing up new apps (before they are capable of passing ELB health checks)",
            "53_Ingress": "Need both UDP and TCP. Clients will try TCP if UDP fails (which can/will happen intermittantly if a domain has a big list of servers)",
            "389_Egress": "Insecure LDAP",
            "636_Egress": "Secure LDAP",
            "587_Egress": "SMTP email",
            "25_Egress": "SMTP email"
          }
        },
        "Properties" : {
          "GroupDescription" : "Enable HTTP access from the ELB and (for now) SSH access from domain.",        
          "SecurityGroupIngress" : [
              {
                "IpProtocol" : "tcp",
                "FromPort" : "800",
                "ToPort" : "815",
                "CidrIp" : "0.0.0.0/0"
              },                 
              {
                "IpProtocol" : "tcp",
                "FromPort" : "5555",
                "ToPort" : "5555",
                "CidrIp" : "0.0.0.0/0"
              },                              
              {
                "IpProtocol" : "tcp",
                "FromPort" : "22",
                "ToPort" : "22",
                "CidrIp" : "0.0.0.0/0"
              }
           ],
           "SecurityGroupEgress" : [
            {
              "IpProtocol" : "tcp",
              "FromPort" : "80",
              "ToPort" : "80",
              "CidrIp" : "0.0.0.0/0"
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "443",
              "ToPort" : "443",
              "CidrIp" : "0.0.0.0/0"
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "587",
              "ToPort" : "587",
              "CidrIp" : "0.0.0.0/0"
            },                                          
            {
              "IpProtocol" : "udp",
              "FromPort" : "53",
              "ToPort" : "53",
              "CidrIp" : {"yac-ref" : "dns-cidr" }
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "53",
              "ToPort" : "53",
              "CidrIp" : {"yac-ref" : "dns-cidr" }
            }
          ],
          "VpcId": {"yac-ref" : "vpc-id"},
          "Tags" : [ {"Key": "Name", "Value" : { "yac-name" : "asg-esg" } },
                     { "Key": "Owner", "Value": { "yac-ref": "owner" } }]
        }
      }
    }
  },
  "service-inputs": {
    "comments": ["Prompt user for these values."],
    "inputs": {   
      "env": {
        "description": "Environment",
        "help":    "The environment to build stack for",            
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": false,
          "options": ["dev","prod"]
        }
      },           
      "tl-domain": {
        "description": "Top-Level Domain",
        "help":    ["The top-level domain the JIRA cname will be aliased. ",
                    "For a nordstrom.com domain, the top-level is 'com'"],
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },      
      "sl-domain": {
        "description": "Second-Level Domain",
        "help":    ["The top-level domain the JIRA cname will be aliased. ",
                    "For a nordstrom.com domain, the second-level is 'nordstrom'"],
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },             
      "vpc-id": {
        "description": "VPC ID",
        "help":    "The ID of the VPC to build stack in",      
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true,
          "options_fxn": {
            "name": "get_vpc_ids"
          }
        }
      },
      "availability-zones": {
        "description": "Availability Zones",
        "help":    "The IDs of the availability zones to build stack in",      
        "wizard_fxn":  {
          "name": "array_wizard"
        },
        "constraints": {
          "required": true,
          "options_fxn": {
            "name": "get_azs"
          }
        }
      },           
      "home-vol-size":  {
        "description": "Home Volume Size",
        "help":    "The size of the EBS home volume (GB). Home is used Jira binaries and home directory (but not attachments).",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      },
      "docker-vol-size":  {
        "description": "Docker Volume Size",
        "help":    "The size of the EBS volume used for Docker images (GB).",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      }, 
      "asg-size":  {
        "description": "ASG Size",
        "help":    "The number of EC2 hosts in the Auto-Scaling group (i.e. Jira Cluster Size).",
        "wizard_fxn":  {
          "name": "int_wizard"
        },
        "constraints": {
          "required": true
        }
      },
      "time-zone":  {
        "description": "Time Zone",
        "help":    "Server time zone, per codes in http://joda-time.sourceforge.net/timezones.html.",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },                                  
      "s3-bucket": {
        "description": "S3 Bucket",
        "help":    "The S3 bucket where backups for this service can be saved to",       
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true,
          "strlen": 20,
          "options_fxn": {
            "name": "get_s3_buckets"
          }
        }
      },            
      "prefix": {
        "description": "Prefix",
        "help":    "A prefix string used for naming and tagging of stack resources",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": false
        }
      },
      "owner":  {
        "description": "Owner",
        "help":    "The stack owner (typically distribution list). Used for notifications",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },      
      "ssh-key":  {
        "description": "SSH Key",
        "help":    "The name of the ssh key pair that can be used for ssh access to host nodes.",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true
        }
      },   
      "instance-ami":  {
        "description": "Instance AMI",
        "help":    "The CoreOS AMI that will be used on Jira's EC2 hosts.",
        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": true,
          "options_fxn": {
            "name": "get_coreos_ami"
          }
        }
      }     
    },
    "conditional-inputs": {
      "dmz-subnet-ids":  {
        "description": "DMZ Subnets Ids",
        "help": ["Ids for subnets that are sandwitched between the public and private subnets.",
                 "Typical VPCs have two or more subnets in this layer."],        
        "comment": ["Subnet Ids for subnets that are sandwitched between the public and private subnets.",
                    "Can only be set once VPC ID is known",
                    "Users are prompted for ids based on the direct-connect setting.",
                    "Ids can alternatively be set via vpc prefs or static params."],

        "wizard_fxn":  {
          "name": "array_wizard",
          "arg":  "dmz"
        },
        "constraints": {
          "required": false,
          "options_fxn": {
            "name": "get_subnet_ids"
          }
        },
        "condition": {
          "input": "direct-connect",
          "value": true
        }                 
      },
      "vpc-cidr":  {
        "description": "VPC CIDR",
        "help": ["The CIDR defining addresses of resources living in our vpcs (used for security group ingress/egress rules)."],        
        "comment": ["JIRA is typically integrated with other resources in the VPC.",
                    "This CIDR is used to create security group rules to let those resources reach Jira, ",
                    " and for JIRA to reach them."],

        "wizard_fxn":  {
          "name": "string_wizard"
        },
        "constraints": {
          "required": false
        },
        "condition": {
          "input": "direct-connect",
          "value": true
        }                 
      },      
      "private-subnet-ids":  {
        "description": "Private Subnets Ids",
        "help": ["Ids for subnets in the private subnet layer.",
                 "Typical VPCs have two or more subnets in this layer."],
        "comment": ["Can only be set once VPC ID is known",
                    "Ids can alternatively be set via vpc prefs or static params."],
        "wizard_fxn":  {
          "name": "array_wizard",
          "arg":  "private"
        },
        "constraints": {
          "required": false,
          "options_fxn": {
            "name": "get_subnet_ids"
          }
        },
        "condition": {
          "input": "vpc-id"
        }                  
      }            
    }
  }  
}