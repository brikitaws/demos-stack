#!/bin/bash

# log boot actions here
LOGFILE=/tmp/boot.log

# configure Confluence to re-write all requests to the FQDN under https
if [ $FQDN ]; then

	# avoid globbing (expansion of *).
	set -f

	# parse the FQDN into its constituent parts     
	array=(${FQDN//./ })

	# CNAME is the DNS cname record for Jira in the FQDN
	# SL_DOMAIN is the second-level domain (e.g. nordstrom)
	# TL_DOMAIN is the top-level domain (e.g. net, com)
	export CNAME=${array[0]}
	export SL_DOMAIN=${array[1]}
	export TL_DOMAIN=${array[2]}

	echo $(date "+%m-%d-%Y %H:%M:%S") "Updating rewrite rules for $FQDN using cname: $CNAME, tl-domain: $TL_DOMAIN, sl-domain: $SL_DOMAIN" >> $LOGFILE

	# copy into place the version of urlrewrite that implements the http-to-https rewrites
	cp $CONFLUENCE_INSTALL/temp/urlrewrite.xml $CONFLUENCE_INSTALL/confluence/WEB-INF/urlrewrite.xml

	# replace tokens in the rewrite file
	sed -i 's/{CNAME}/'"$CNAME"'/g'   $CONFLUENCE_INSTALL/confluence/WEB-INF/urlrewrite.xml
	sed -i 's/{TL_DOMAIN}/'"$TL_DOMAIN"'/g' $CONFLUENCE_INSTALL/confluence/WEB-INF/urlrewrite.xml
	sed -i 's/{SL_DOMAIN}/'"$SL_DOMAIN"'/g' $CONFLUENCE_INSTALL/confluence/WEB-INF/urlrewrite.xml
fi

# clear the plugin caches - these can sometimes interfere with server startup
rm -R $CONFLUENCE_HOME/plugins-cache
rm -R $CONFLUENCE_HOME/plugins-osgi-temp      
rm -R $CONFLUENCE_HOME/plugins-temp
rm -R $CONFLUENCE_HOME/bundled-plugins

# Load certs into cacerts for other hosts Confluence needs a trust-relationship with. Do this before every startup
# such that new certs can be picked up by simply restarting container. 
# note: bash insists on a space between parens and variable in the if statements
if [ $JIRA_HOST ]; then
    # download the Jira cert for this env from the Jira host to a temp file and load into truststore
    openssl s_client -showcerts -connect $JIRA_HOST:443 </dev/null 2>/dev/null | openssl x509 -outform DER > /tmp/jira.der
    printf 'changeit\nyes\n' | keytool -import -alias $JIRA_HOST -keystore /usr/lib/jvm/java-7-oracle/jre/lib/security/cacerts -file /tmp/jira.der
fi

# Run Confluence in foreground under the daemon user
exec su daemon -s /bin/bash -c './bin/start-confluence.sh -fg'